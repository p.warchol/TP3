// Port to listen requests from
var port = 1234;

// Modules to be used
var express = require('express');
var bodyParser = require('body-parser');
var sqlite3 = require('sqlite3').verbose();
var app = express();
var db = new sqlite3.Database('db.sqlite');
var cookie = require('cookie');

// application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false });

// Log requests
app.all("*", urlencodedParser, function(req, res, next){
	console.log(req.method + " " + req.url);
	console.dir(req.headers);
	console.log(req.body);
	console.log();
	next();
});

// Serve static files
app.use(express.static('public'));

app.get("/users", function(req, res, next) {
	db.all('SELECT rowid, ident, password FROM users;', function(err, data) {
		res.json(data);
	});
});

app.post("/login", function(req, res, next) {
	db.all("SELECT * FROM users where ident=? and password=?;", [req.body.login, req.body.password], function(err,data){
		if (data.length != 0){
			var ident = data[0].ident;
			var tok1 = Math.random().toString(36).substr(2);
			var tok2 = Math.random().toString(36).substr(2);
			var token = tok1 + tok2;
			var request = "INSERT into sessions VALUES(?,?)";
			db.run(request, [ident, token]);

			res.setHeader('Set-Cookie', cookie.serialize('cookie_session', token, {
      httpOnly: true,
      maxAge: 60 * 60 * 24 * 7 // 1 week
    	}));

			res.json("status: true, session: " + token);

			res.statusCode = 302;
			res.end();
		}
		else{
			res.json("status: false");
		}
	});
});

// Startup server
app.listen(port, function () {
	console.log('Le serveur est accessible sur http://localhost:' + port + "/");
	console.log('La liste des utilisateurs est accessible sur http://localhost:' + port + "/users");
});
